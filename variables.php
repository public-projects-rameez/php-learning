<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day 1-php</title>
    <?php include("includes/style.php"); ?>

</head>

<body class="container">
    <?php include("includes/header.php"); ?>
    <h4>Global variables</h4>
    <?php
    $x = 10;
    $y = 20;
    $z = 60;

    function myTest()
    {
        global $x, $y;
        $z = $x + $y;
        return $z;
    }

    echo "Hello 23-10-2023!";
    echo "<br>";
    echo "\$x: $x";
    echo "<br>";
    echo "\$y: $y";
    echo "<br>";
    echo "\$z: $z";
    echo "<br>";

    echo "<p>Double Qoute \$x: $x</p>";
    echo '<p>Single Qoute $y: $y</p>';
    echo "myTest() \$z:" . myTest();
    echo "<br>";
    echo "Global \$z: $z";
    ?>

</body>

</html>