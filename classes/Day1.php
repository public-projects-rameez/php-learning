<?php

class Day1
{
    // public function __construct(){}
    private $x = 10;
    private $y = 20;
    private $z = 60;

    function myTest()
    {
        global $x, $y;
        $z = $x + $y;
        return $z;
    }


    public function index()
    {
        global $z;

        echo "Hello 23-10-2023!";

        echo "<br>";
        echo "\$x: $this->x";
        echo "<br>";
        echo "\$y: $this->y";
        echo "<br>";
        echo "\$z: $this->z";
        echo "<br>";
        echo "<br>";

        $this->x = 2;

        echo "Modified by Day1->index()";
        echo "<br>";
        echo "\$x: $this->x";
        echo "<br>";
        echo "\$y: $this->y";
        echo "<br>";
        echo "\$z: $this->z";
        echo "<br>";
        echo "<br>";

        echo "myTest() \$z:" . myTest();
        echo "<br>";
        echo "Global \$z: " . $z;
    }

    public function __toString()
    {
        return "\$x: $this->x" . "<br>" . "\$y: $this->y" . "<br>" . "\$z: $this->z" . "<br>";
    }
}
