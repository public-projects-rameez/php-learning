<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include("includes/style.php"); ?>

</head>

<body class="container">
    <?php include("includes/header.php"); ?>
    <h4>Data Types</h4>
    <pre class="code">
$x = 10.365;
echo $x;

var_dump(is_float($x));
var_dump(is_int($x));

$x = 1.9e411;
var_dump($x);

$x = 5985;
echo $x;
var_dump(is_numeric($x));

$x = "5985";
echo $x;
var_dump(is_numeric($x));

$x = "59.85" + 100;
echo $x;
var_dump(is_numeric($x));

$x = "Hello";
echo $x;
var_dump(is_numeric($x));
echo ("rand(10, 100): ");
echo (rand(10, 100));      
    </pre>

    <p>Output</p>
    <div class="output">
        <?php

        $x = 10.365;
        echo $x;
        echo "<br>";

        var_dump(is_float($x));
        echo "<br>";
        var_dump(is_int($x));
        echo "<br>";

        $x = 1.9e411;
        var_dump($x);
        echo "<br>";

        $x = 5985;
        echo $x;
        echo "<br>";
        var_dump(is_numeric($x));
        echo "<br>";

        $x = "5985";
        echo $x;
        echo "<br>";
        var_dump(is_numeric($x));
        echo "<br>";

        $x = "59.85" + 100;
        echo $x;
        echo "<br>";
        var_dump(is_numeric($x));
        echo "<br>";

        $x = "Hello";
        echo $x;
        echo "<br>";
        var_dump(is_numeric($x));
        echo "<br>";
        echo ("rand(10, 100): ");
        echo (rand(10, 100));
        echo "<br>";
        echo "<br>";
        echo "<br>";
        echo "<br>";
        ?>
    </div>





</body>

</html>