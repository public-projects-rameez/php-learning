<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>W3 PHP-Notes</title>

    <?php include("includes/style.php"); ?>
    <style>
        h2 {
            position: sticky;
            top: 50px;
            background-color: white;
        }

        .code-block {}
    </style>
</head>

<body>
    <main class="container">
        <?php include("includes/header.php"); ?>
        <h1>PHP Notes</h1>
        <p>Prepared By: <b>Rana Muhammad Rameez</b> | <a href="mailto:mrameezrana99@gmail.com">mrameezrana99@gmail.com</a> | 0324 4085220</p>
        <!-- 
            <div class="card my-2 p-3">
                <h2>Topic</h2>
                <ul>
                </ul>
            </div>
        -->


        <!-- Date -->
        <div class="card my-2 p-3">
            <?php
            $t = date("H");

            if ($t < "10") {
                echo "Have a good morning!";
            } elseif ($t < "20") {
                echo "Have a good day!";
            } else {
                echo "Have a good night!";
            }
            ?>
        </div>

        <!-- Variables -->
        <div class="card my-2 p-3">
            <h2>Variables</h2>
            <ul>
                <li>Double qouted string can interpret variables. Single can't!</li>
            </ul>
        </div>


        <!-- PHP Logical Operators -->
        <div class="card my-2 p-3">
            <h2>PHP Logical Operators</h2>
            <ul>
                <li>And: and , &&</li>
                <li>Or: or , ||</li>
                <li>Xor: xor</li>
                <li>Not: !</li>
            </ul>
        </div>

        <!-- Foreach Loop -->
        <div class="card my-2 p-3">
            <h2>Foreach Loop</h2>
            <ul>
                <li>
                    <p>Value only loop</p>
                    <?php
                    $colors = array("red", "green", "blue", "yellow");

                    foreach ($colors as $value) {
                        echo "$value <br>";
                    }
                    ?>
                </li>
                <li>
                    <p>Key Value loop (works Associative Array)</p>
                    <?php
                    $age = array("Peter" => "35", "Ben" => "37", "Joe" => "43");

                    foreach ($age as $x => $val) {
                        echo "$x = $val<br>";
                    }
                    ?>
                </li>
            </ul>
        </div>

        <!-- Functions -->
        <div class="card my-2 p-3">
            <h2>Functions</h2>
            <ul>
                <li>Function names are NOT case-sensitive.</li>
                <li>writeMsg();</li>
                <li>WriteMsg();</li>
                <li>
                    <?php
                    function writeMsg()
                    {
                        echo "writeMsg() called<br>";
                    }

                    writeMsg(); // call the function
                    WriteMsg(); // call the function
                    ?>
                </li>
            </ul>
        </div>

        <!-- Super Globals -->
        <div class="card my-2 p-3">
            <h2>Super Globals</h2>
            <ol>
                <li> $GLOBALS</li>
                <li> $_SERVER</li>
                <li>$_REQUEST</li>
                <li> $_POST</li>
                <li> $_GET</li>
                <li> $_FILES</li>
                <li> $_ENV</li>
                <li> $_COOKIE</li>
                <li>$_SESSION</li>
            </ol>
        </div>

        <!-- $GLOBALS -->
        <div class="card my-2 p-3">
            <h2>$GLOBALS</h2>
            <h3>$_SERVER[]</h3>
            <section>
                <p>Access the $_SERVER array using: <code>$_SERVER['key']</code> like
                    <code>$_SERVER['PHP_SELF']</code>. <br>
                    Some Important keys are given below.
                </p>
                <ol>
                    <li>PHP_SELF</li>
                    <li>SERVER_NAME</li>
                    <li>HTTP_HOST</li>
                    <li>HTTP_REFERER</li>
                    <li>HTTP_USER_AGENT</li>
                    <li>SCRIPT_NAME</li>
                    <li>SERVER_SIGNATUR</li>
                    <li>PATH_TRANSLATE</li>
                    <li>SCRIPT_URI</li>
                    <li>SERVER_POR</li>
                    <li>HTTP</li>
                    <li>QUERY_STRIN</li>
                    <li>REQUEST_METHOD</li>
                </ol>
                <p>Output</p>
                <div class="code-block card bg-dark py-2 px-3">
                    <code>
                        <?php

                        function showServerFeild($fieledName)
                        {
                            echo isset($_SERVER[$fieledName]) ? "<b>\$_SERVER[$fieledName]</b>: $_SERVER[$fieledName]" : "\$_SERVER['$fieledName'] not found.";
                            echo "<br>";
                        }

                        showServerFeild('PHP_SELF');
                        showServerFeild('SERVER_NAME');
                        showServerFeild('HTTP_HOST');
                        showServerFeild('HTTP_REFERER');
                        showServerFeild('HTTP_USER_AGENT');
                        showServerFeild('SCRIPT_NAME');
                        showServerFeild('SERVER_SIGNATURE');
                        showServerFeild('PATH_TRANSLATED');
                        showServerFeild('SCRIPT_URI');
                        showServerFeild('SERVER_PORT');
                        showServerFeild('HTTPS');
                        showServerFeild('QUERY_STRING');
                        showServerFeild('REQUEST_METHOD');
                        ?>
                    </code>
                </div>
            </section>

        </div>

        <!-- 
            <div class="card my-2 p-3">
                <h2>Topic</h2>
                <ul>
                </ul>
            </div>
        -->




        <!-- Constants -->
        <div class="card my-2 p-3">
            <h2>Constants</h2>
            <ul>
                <li>define("I_AM_CONSTANT", "Constant Value");</li>
                <li>Doesn't need $</li>
                <li>From PHP 7.3 all constants are case-sensitive</li>
                <li>case-insensitive: Specifies whether the constant name should be case-insensitive. Default is false.
                    Note: Defining case-insensitive constants was deprecated in PHP 7.3. PHP 8.0 accepts only false, the
                    value true will produce a warning.</li>
            </ul>

            <h3>Try case insensitive</h3>
            <pre class="code">
define("GREETING", "Constant named 'GREETING'");
define("doesnt_matter", "Constant named 'doesnt_matter'", true);
echo doesnt_matter;
echo Doesnt_Matter;
echo GREETING;
echo greeting; 
</pre>
            <p>Output</p>
            <div class="output">

                <?php
                define("GREETING", "Constant named 'GREETING'");
                define("doesnt_matter", "Constant named 'doesnt_matter'", true);
                echo doesnt_matter;
                echo "<br>";
                echo Doesnt_Matter;
                echo "<br>";

                echo GREETING;
                echo "<br>";
                echo greeting;
                ?>
            </div>
        </div>


    </main>

</body>

</html>