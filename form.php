<!DOCTYPE html>

<head>
    <?php include("includes/style.php"); ?>

</head>
<html>
<!-- this form is submitted to this page itself -->

<body class="container">
    <?php include("includes/header.php"); ?>
    <p>This form is submitted to this page itself</p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
        Name: <input type="text" name="fname">
        <input type="submit">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // collect value of input field
        $name = htmlspecialchars($_REQUEST['fname']);
        if (empty($name)) {
            echo "Name is empty";
        } else {
            echo $name;
        }
    }
    ?>

</body>

</html>