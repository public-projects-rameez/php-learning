<!DOCTYPE html>
<html lang="en">
<?php
require_once 'classes/Day1.php';
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>w3 php</title>
    <?php include("includes/style.php"); ?>

</head>

<body class="container">
    <?php include("includes/header.php"); ?>
    <h4>Regular Expression</h4>
    <p>Methods</p>
    <ul>
        <li>preg_match()</li>
        <li>preg_match_all()</li>
        <li>preg_replace()</li>
    </ul>
    <p>List of modifiers</p>
    <ul>
        <li>i</li>
    </ul>
    <div>
        <p>Use a regular expression to do a case-insensitive search for "w3schools" in a string:</p>
        <p class="code">$str = "Visit W3Schools";</p>

        <pre class="code">
$pattern = "/w3schools/i";
echo preg_match($pattern, $str); // Outputs 1
</pre>

        <!-- output div start -->
        <div>
            Output:
            <div class="output">
                <?PHP
                $str = "Visit W3Schools";
                $pattern = "/w3schools/i";
                echo preg_match($pattern, $str); // Outputs 1
                ?>
            </div>
        </div>
        <!-- output div end -->

        <p>Use a regular expression to do a case-sensitive search for "w3schools" in a string:</p>
        <pre class="code">
$pattern = "/w3schools/";
echo preg_match($pattern, $str); // Outputs 0
</pre>

        <!-- output div start -->
        <div>
            Output:
            <div class="output">
                <?PHP
                $pattern = "/w3schools/";
                echo preg_match($pattern, $str); // Outputs 1
                ?>
            </div>
        </div>
        <!-- output div end -->

    </div>

    <h4>String Test</h4>
    <?php
    $x = 50;
    $y = 100;
    echo "<p>Double Qoute \$x: $x</br>";
    echo 'Single Qoute $y: $y</p>';
    ?>

    <h4>Class object</h4>
    <?php
    $day1 = new Day1();
    echo $day1->index();
    echo $day1;
    ?>

    <h4>Static variable in a function</h4>
    <?php
    function myTest()
    {
        static $x = 0;
        echo $x;
        echo "<br>";
        $x++;
    }

    myTest();
    myTest();
    myTest();
    ?>

    <h4>Strings</h4>
    <?php
    echo strlen("Hello world!"); // outputs 12
    echo "<br>";
    echo str_word_count("Hello world!"); // outputs 2
    echo "<br>";
    echo strrev("Hello world!"); // outputs !dlrow olleH
    echo "<br>";
    echo strpos("Hello world!", "world"); // outputs 6
    echo "<br>";
    echo str_replace("world", "Dolly", "Hello world!"); // outputs Hello Dolly!
    echo "<br>";

    ?>

</body>

</html>