<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include("includes/style.php"); ?>
</head>

<body class="container">
    <?php include ("includes/header.php");?>
    <h1>Notes on PHP Form Security and Validation</h1>
    <h4>Form Security</h4>
    <p><code>Security</code> checks for form inputs in php</p>
    <ol>
        <li><code>htmlspecialchars()</code> can be used to replace unwanted characters like <> to & lt; & gt;</li>
        <li><b>Strip tags (remove extra spaces):</b> Strip unnecessary characters (extra space, tab, newline) from the user input data (with the PHP trim() function)</li>
        <li><b>Strip backslashes:</b> Remove backslashes (\) from the user input data (with the PHP stripslashes() function)</li>
    </ol>
    <p>So  we can make a standard function for <em>sanatizing our inputs</em></p>
    <pre class="code">
function sanatize_input_field($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
    </pre>

    <h4>Form Validation</h4>
    <div>

    </div>
</body>

</html>