<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>

<style>
    body {
        padding: 0.1rem 1.5rem;
    }

    header {
        padding: 10px;
        background-color: #7c6;
        position: sticky;
        color: white !important;
        top: 0;
        z-index: 999;
    }

    header nav ul {
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 1rem;
    }

    header nav ul {
        margin: 0 !important;
        padding: 0 Im !important;
    }

    header nav li {
        display: inline;
        padding: 4px 10px;
        opacity: 0.9;
        border-radius: 3px;
        margin: 0 !important;
        padding: 0 Im !important;
    }

    header nav li:hover {
        opacity: 1;
    }

    header a {
        text-decoration: none;
        color: white !important;
    }

    h4 {
        margin-top: 1rem;
    }

    .code {
        background-color: #555;
        color: white;
        padding: 10px;
        margin: 1rem 0;
        border-radius: 15px;

    }

    .output {
        background-color: #111;
        color: white;
        padding: 10px;
        margin: 1rem 0;
        border-radius: 15px;
    }
</style>