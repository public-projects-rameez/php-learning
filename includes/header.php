<header>
    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="security.php">Security</a></li>
            <li><a href="regex.php">Regex</a></li>
            <li><a href="form.php">Form</a></li>
            <li><a href="data_types.php">Data Types</a></li>
            <li><a href="variables.php">Variables</a></li>
        </ul>
    </nav>
</header>